var slideModal = (function(){

    return {
      init: function(){
        $("body").on( "click", "[data-slide_content]", function(event){


          event.preventDefault();
          var modal = $(this).data("slide_content");

          slideModal.modalOpen(modal);
        });
        $("body").on( "click", function(event){
          var target = $(event.target);
          if ( target.hasClass('team_popup__btn')){
            slideModal.modalClose();
          }
        });
        $("body").on( "keydown", function(event){
          if (event.keyCode == 27) {
            slideModal.modalClose();
          }
        });
      },

      /* public methods */
      modalOpen: function(modal){
        $('.'+modal).fadeIn();
        /*overlayOpen();*/
      },
      modalClose: function(){
        $(".team_popup").fadeOut();
       /* overlayClose();*/
      },
    }
  })();