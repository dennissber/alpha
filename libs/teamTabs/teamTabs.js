var teamTabs = (function(){
    var option = {
          dots: false,
          infinite: true,
          //autoplay: true,
          //autoplaySpeed: 3000,
          speed: 3000,
          slidesToShow: 1,
          slidesToScroll: 1,
          dotsClass: 'slick-dots',
        };
    return {
      init: function(){
        $("body").on( "click", "[data-team_tabs]", function(event){
          event.preventDefault();

          var target = $(event.target);
          var modal = $(this).data("team_tabs");
          
          teamTabs.modalOpen(modal);
         
        });
        $('.fn_team_tabs_1').slick(option);
      },

      /* public methods */
      modalOpen: function(modal){
       
        //var teamTabs = $('.team_slider');
        var teamSlides = document.querySelectorAll('.team_tabs');

        var j;
        for (j=0; j<teamSlides.length; j++){
          var teamSlidesItem = teamSlides[j];
          $(teamSlidesItem).removeClass('show');
        };

        var slider = $('.'+modal).find('.team_slider');
        var classSlider = 'fn_'+modal;

        $('.'+modal).addClass('show');

        if($(slider).hasClass(classSlider)) {
          $(slider).slick('unslick');
          $(slider).slick(option);
        } else {
           $(slider).addClass(classSlider);
           $(slider).slick(option);
        }
      },
      
      //modalClose: function(){
       // $(".team_popup").fadeOut();
      //},
    }
})();

var teamTabsMobile = (function(){
    var optionMobile = {
          centerMode: true,
          arrows: true,
          centerPadding: '200px',
          slidesToShow: 1,
          infinite: true,
          responsive: [
            {
              breakpoint: 768,
              settings: {
                centerPadding: '100px',
              }
            },
            {
              breakpoint: 576,
              settings: {
                centerPadding: '80px',
              }
            },
            {
              breakpoint: 400,
              settings: {
                centerPadding: '40px',
              }
            }
          ]
        };

    return {
      init: function(){
        $("body").on( "click", "[data-team_tabs_m]", function(event){
          event.preventDefault();

          var target = $(event.target);
          var modal = $(this).data("team_tabs_m");
        
          teamTabsMobile.modalOpen(modal);
          
        });
        $('.fn_team_tabs_m_1').slick(optionMobile);
      },

      /* public methods */
      modalOpen: function(modal){

        //var teamTabs = $('.team_slider');
        var teamSlides = document.querySelectorAll('.team_tabs_m');

        var j;
        for (j=0; j<teamSlides.length; j++){
          var teamSlidesItem = teamSlides[j];
          $(teamSlidesItem).removeClass('show');
        };

        var slider = $('.'+modal).find('.team_slider_m');
        var classSlider = 'fn_'+modal;

        $('.'+modal).addClass('show');

        if($(slider).hasClass(classSlider)) {
          $(slider).slick('unslick');
          $(slider).slick(optionMobile);
        } else {
           $(slider).addClass(classSlider);
           $(slider).slick(optionMobile);
        }
      },
      
      //modalClose: function(){
       // $(".team_popup").fadeOut();
      //},
    }
})();