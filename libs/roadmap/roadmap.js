var roadmap = (function(){

    return {
      init: function(){
        $("body").on( "click", "[data-roadmap_content]", function(event){
          event.preventDefault();
          var target = $(event.target);
          var modal = $(this).data("roadmap_content");
          if (target.hasClass('roadmap_item__blue')) {
            roadmap.modalOpenBlue(modal);
          } else if (target.hasClass('roadmap_item__yellow')) {
            roadmap.modalOpenYellow(modal);
          }
        });
        /*$("body").on( "click", function(event){
          var target = $(event.target);
          if ( target.hasClass('team_popup__btn')){
            slideModal.modalClose();
          }
        });
        $("body").on( "keydown", function(event){
          if (event.keyCode == 27) {
            slideModal.modalClose();
          }
        });*/
      },

      /* public methods */
      modalOpenBlue: function(modal){
        var blueContent = $('.roadmap_road__list > .roadmap_road');

        var i;
        for (i=0; i<blueContent.length; i++){
          var blueContentItem = blueContent[i];
          $(blueContentItem).css('display', 'none');
        };

        $('.'+modal).fadeIn();
      },
      modalOpenYellow: function(modal){
        var yellowContent = $('.roadmap_funding__list > .roadmap_funding');
        var j;
        for (j=0; j<yellowContent.length; j++) {
          var yellowContentItem = yellowContent[j];
          $(yellowContentItem).css('display', 'none');
        };
        $('.'+modal).fadeIn();
      },
      //modalClose: function(){
       // $(".team_popup").fadeOut();
      //},
    }
  })();