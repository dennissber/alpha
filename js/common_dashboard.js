$(function () {
    /* modals */
    function modalOpen(modal, target) {
        $('.' + modal).slideDown();
        if (target.hasClass('indicators_item')) {
            $(target).addClass('indicators_item__show');
        } else if (target.parent().hasClass('indicators_item')) {
            target.parent().addClass('indicators_item__show');
        }

        //overlayOpen();
    }

    function modalClose(modal) {
        $('.' + modal).slideUp();
        //overlayClose();
    }

    $("body").on("click", "[data-table]", function (event) {
        event.preventDefault();
        var modal = $(this).attr("data-table");
        var target = $(event.target);
        if (target.hasClass('indicators_item__show') || target.parent().hasClass('indicators_item__show')) {
            modalClose(modal);
            $(target).removeClass('indicators_item__show');
            target.parent().removeClass('indicators_item__show');
        } else if (target.hasClass('indicators_item') || target.parent().hasClass('indicators_item')) {
            modalOpen(modal, target);
        }
    });

    /* masked input */
    //var mask = "+7 (999) 999-99-99";
    //var placeholder = {'placeholder':'+7 (___) ___ __ __'};
    var mask = "+38(099) 999-99-99";
    var placeholder = {'placeholder': '+38(0__) ___-__-__'};
    var user_phone = $('.user_phone_mask');

    user_phone.each(function () {
        $(this).mask(mask);
    });
    user_phone.attr(placeholder);

    // actions
    $(window).on('resize', function () {

    });

    $(window).on('load', function () {

    });

    $(window).on('scroll', function () {

        if ($(window).scrollTop()) {
            $('.dashboard_header').addClass('scrolling');
        } else {
            $('.dashboard_header').removeClass('scrolling');
        }

        if ($(window).width() <= '991') {
        }
        ;
    });

    $('.header_nav__btn').on('click', function () {
        $('.header_nav__holder').toggleClass('menu_state_open');
        $('.header_nav__btn').toggleClass('nav_btn__active');
        $('body').toggleClass('no_scroll');
    });

    /*$('.fn_underperform_slider').slick({
      vertical: true,
      verticalSwiping: true,
      initialSlide: 1,
      adaptiveHeight: true,
      centerMode: true,
      centerPadding: '0px',
      dots: false,
      arrows: true,
      infinite: true,
      fade: false,
      //autoplay: true,
      //autoplaySpeed: 3000,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      dotsClass: 'slick-dots',
    });

    $('.fn_outperform_slider').slick({
      vertical: true,
      verticalSwiping: true,
      initialSlide: 1,
      adaptiveHeight: true,
      centerMode: true,
      centerPadding: '0px',
      dots: false,
      arrows: true,
      infinite: true,
      fade: false,
      //autoplay: true,
      //autoplaySpeed: 3000,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      dotsClass: 'slick-dots',
    });

    $('.fn_when_slider').slick({
      vertical: true,
      verticalSwiping: true,
      initialSlide: 1,
      adaptiveHeight: true,
      centerMode: true,
      centerPadding: '0px',
      dots: false,
      arrows: true,
      infinite: true,
      fade: false,
      //autoplay: true,
      //autoplaySpeed: 3000,
      speed: 500,
      slidesToShow: 2,
      slidesToScroll: 1,
      dotsClass: 'slick-dots',
    });*/


    ////////// Indicators functionality //////////

    $('.fn_indicators__slider').slick({
        vertical: true,
        verticalSwiping: true,
        initialSlide: 1,
        adaptiveHeight: true,
        centerMode: true,
        centerPadding: '0px',
        dots: false,
        arrows: true,
        infinite: true,
        fade: false,
        //autoplay: true,
        //autoplaySpeed: 3000,
        speed: 1500,
        slidesToShow: 2,
        slidesToScroll: 1,
        dotsClass: 'slick-dots',
    });

    var tablesList = document.querySelectorAll('.indicators_table__holder');
    $('.fn_indicators__slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        var i;
        for (i = 0; i < tablesList.length; i++) {
            var currentTable = tablesList[i];
            $(currentTable).hide();
        }
        $(tablesList[currentSlide]).fadeIn(1500);
    });

    ///////// News functionality ////////

    $(window).resize(function () {
        $('.fn_news_tw__slider').slick('resize');
        $('.fn_news_stock__slider').slick('resize');
        $('.fn_news_google__slider').slick('resize');
    });

    $(window).on('orientationchange', function () {
        $('.fn_news_tw__slider').slick('resize');
        $('.fn_news_stock__slider').slick('resize');
        $('.fn_news_google__slider').slick('resize');
    });

    var newsTabs = (function () {
        var option = {
            dots: false,
            arrows: true,
            infinite: true,
            speed: 1500,
            slidesToShow: 3,
            slidesToScroll: 1,
            dotsClass: 'slick-dots',
            responsive: [
                {
                    breakpoint: 992,
                    settings: 'unslick'
                },
            ]
        };
        return {
            init: function () {
                $("body").on("click", "[data-news_tab]", function (event) {
                    event.preventDefault();

                    var target = $(event.target);
                    var modal = $(this).data("news_tab");

                    var newsBtns = document.querySelectorAll('.news_tabs__btn');
                    var i;
                    for (i = 0; i < newsBtns.length; i++) {
                        var newsBtnsItem = newsBtns[i];
                        $(newsBtnsItem).removeClass('active');
                    }
                    ;
                    $(target).addClass('active');

                    newsTabs.modalOpen(modal);

                });
                $('.fn_news_tw__slider').slick(option);
            },

            /* public methods */
            modalOpen: function (modal) {

                var newsSlides = document.querySelectorAll('.news_slide__list');

                var j;
                for (j = 0; j < newsSlides.length; j++) {
                    var newsSlidesItem = newsSlides[j];
                    $(newsSlidesItem).removeClass('show');
                }
                ;

                var currentSlider = $('.' + modal)
                //var slider = $('.'+modal).find('.news_slider');
                var classSlider = 'fn_' + modal;

                $('.' + modal).addClass('show');

                if ($(currentSlider).hasClass(classSlider)) {
                    $(currentSlider).slick('unslick');
                    $(currentSlider).slick(option);
                    //console.log(currentSlider);
                } else {
                    $(currentSlider).addClass(classSlider);
                    $(currentSlider).slick(option);
                }
            },
        }
    })();

    newsTabs.init();

    $(".news_slide__list").niceScroll({
        cursorcolor: "#45bdb3",
        cursorwidth: "2px",
        cursorborder: "none",
        cursoropacitymin: 1,
    });

    /////// Underperform, Outperfom functionality //////////
    function contentOpen(modal, target) {
        reinitScroll();
        $('.' + modal).slideDown();
        if (target.hasClass('left_col__item_title')) {
            $(target).addClass('show');
        }

        if ($('.underperform_title').hasClass('show')) {
            $('.recommend_link__underperform').addClass('active');
        } else if ($('.outperform_title').hasClass('show')) {
            $('.recommend_link__outperform').addClass('active')
        }
    }

    function contentClose(modal) {
        $('.' + modal).slideUp();
        var currentItems = $('.' + modal).find('li');
        for (var i = 0; i < currentItems.length; i++) {
            $(currentItems[i]).removeClass('active');
        }
        $('.when_title').removeClass('show');
        $('.content_show').slideUp();
        $('.why_title').removeClass('show');
        $('.list_show').slideUp();
        $('.why_btn').removeClass('why_btn__show');
        if ($('.list_show').hasClass('slick-slider')) {
            $('.list_show').slick('unslick');
        }
        $('.recommend_box').removeClass('d-flex');
        $('.recommend_box').slideUp();
        $('.recommend_link__underperform').removeClass('active');
        $('.recommend_link__outperform').removeClass('active');
        reinitScroll();
    }

    function listOpen(list) {
        var lists = $('.why_list');
        for (var i = 0; i < lists.length; i++) {
            $(lists[i]).removeClass('list_show');
        }
        $('.' + list).addClass('list_show');
    }


    $("body").on("click", "[data-table]", function (event) {
        event.preventDefault();

        var whyList = $(this).attr("data-why");
        listOpen(whyList);

        var modal = $(this).attr("data-table");
        var target = $(event.target);

        if (target.hasClass('show')) {
            contentClose(modal);
            $(target).removeClass('show');
        } else if (target.hasClass('left_col__item_title')) {
            var modals = $('.left_col__item_title');
            var activeModal;
            for (var j = 0; j < modals.length; j++) {
                if ($(modals[j]).hasClass('show')) {
                    activeModal = true;
                }
            }
            if (!activeModal) {
                contentOpen(modal, target);
            }
        }

    });

    $('.left_col__content > ul > li').on("click", function (event) {
        event.preventDefault();
        var target = event.target;
        var contentItems = $(target).parent().find('li');
        for (var i = 0; i < contentItems.length; i++) {
            $(contentItems[i]).removeClass('active');
        }
        $(this).toggleClass('active');
    });

    $('.left_col__btn').on("click", function (event) {
        event.preventDefault();
        reinitScroll();
        var items = $('.left_col__content > ul > li');
        for (var i = 0; i < items.length; i++) {
            var activeItem = items[i];
            if ($(activeItem).hasClass('active')) {
                $('.when_title').addClass('show');
                $('.content_show').slideDown();
                var whenItems = $('.when_item');
                $(whenItems).each(function () {
                    $(this).removeClass('active');
                })
            }
        }
    })

    var optionWhySlider = {
        waitForAnimate: true,
        adaptiveHeight: true,
        dots: false,
        arrows: true,
        infinite: true,
        fade: true,
        //autoplay: true,
        //autoplaySpeed: 3000,
        speed: 1500,
        slidesToShow: 1,
        slidesToScroll: 1,
        dotsClass: 'slick-dots',
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    fade: false,
                    centerMode: true,
                    centerPadding: '80px',
                }
            },
            {
                breakpoint: 460,
                settings: {
                    fade: false,
                    centerMode: true,
                    centerPadding: '40px',
                }
            },
        ]
    };

    $('.when_btn').on("click", function (event) {
        event.preventDefault();
        reinitScroll();
        var items = $('.when_item');
        for (var i = 0; i < items.length; i++) {
            var activeItem = items[i];
            if ($(activeItem).hasClass('active')) {
                $('.why_title').addClass('show');
                $('.list_show').slideDown();
                $('.why_btn').addClass('why_btn__show');
                if ($('.list_show').hasClass('slick-slider')) {
                    $('.list_show').slick('unslick');
                }
                $('.list_show').slick(optionWhySlider);
            }
        }
    });

    $('.why_btn').on("click", function (event) {
        event.preventDefault();
        reinitScroll();
        $('.recommend_box').slideDown();
        $('.recommend_box').addClass('d-flex');
    });

    function reinitScroll() {
        $(".news_slide__list").getNiceScroll().remove()
        setTimeout(function () {
            $(".news_slide__list").niceScroll({
                cursorcolor: "#45bdb3",
                cursorwidth: "2px",
                cursorborder: "none",
                cursoropacitymin: 1,
            });
        }, 500);
    }


});

$('.tabs').each(function(i) {
    var cur_nav = $(this).find('.tab_navigation'),
        cur_tabs = $(this).find('.tab_container');
    if(cur_nav.children('.selected').length > 0) {
        $(cur_nav.children('.selected').attr("href")).show();
        cur_tabs.css('height', cur_tabs.children($(cur_nav.children('.selected')).attr("href")).outerHeight());
    } else {
        cur_nav.children().first().addClass('selected');
        cur_tabs.children().first().show();
        cur_tabs.css('height', cur_tabs.children().first().outerHeight());
    }
});
$('.tab_navigation_link').click(function(e){
    e.preventDefault();
    if($(this).hasClass('selected')){
        return true;
    }
    var cur_nav = $(this).closest('.tabs').find('.tab_navigation'),
        cur_tabs = $(this).closest('.tabs').find('.tab_container');
    cur_tabs.children().hide();
    cur_nav.children().removeClass('selected');
    $(this).addClass('selected');
    $($(this).attr("href")).fadeIn(200);
    cur_tabs.css('height', cur_tabs.children($(this).attr("href")).outerHeight());
});