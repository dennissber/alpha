$(function () {
    var App = {
        height: $(window).height(),
        width: $(window).width(),
        navHeight: 50,
    }

    $(window).on('scroll', function () {

        if ($(window).scrollTop()) {
            $('.nav_btn__main').addClass('nav_btn__scrolling');
        } else {
            $('.nav_btn__main').removeClass('nav_btn__scrolling');
        }

        if ($(window).width() <= '991') {
        }
        ;
    });

    /* masked input */
    var mask = "+7 (999) 999-99-99";
    var placeholder = {'placeholder': '+7 (___) ___ __ __'};
    //var mask = "+38(099) 999-99-99";
    //var placeholder = {'placeholder':'+38(0__) ___-__-__'};
    var user_phone = $('.user_phone_mask');

    user_phone.each(function () {
        $(this).mask(mask);
    });
    user_phone.attr(placeholder);

    // actions
    $(window).on('resize', function () {

    });
    $(window).on('scroll', function () {

    });
    $(window).on('load', function () {

    });


    $('.fn_roadmap_mobile__slider').slick({
        centerMode: true,
        arrows: true,
        centerPadding: '200px',
        slidesToShow: 1,
        infinite: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    centerMode: true,
                    centerPadding: '100px',
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 576,
                settings: {
                    centerMode: true,
                    centerPadding: '100px',
                    slidesToShow: 1
                }
            }
        ]
    });


    $('.fn_roadmap_mobile__slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        if (nextSlide <= 2) {
            $('.roadmap_mobile__item').html('2017');
        } else if (nextSlide >= 3) {
            $('.roadmap_mobile__item').html('2018');
        }
    });

    function openTokensPopup() {
        $('.tokens_popup').fadeIn();
        setTimeout(showTokenPopupBtn, 5000);
    }

    function showTokenPopupBtn() {
        $('.tokens_popup__btn').fadeIn();
    }

    $(".tokens_popup__btn").on("click", function (event) {
        event.preventDefault();
        $('.tokens_popup').fadeOut();
    });

    setTimeout(openTokensPopup, 5000);

    /*$(document).ready( function() {

      if ( $(window).width() < 991) {
       $('.team_list').addClass('owl-carousel');
        $('.owl-carousel').owlCarousel({
        center: true,
        items:2,
        loop:true,
        margin:50,
        dots: false,
        autoWidth: true,
        responsive:{
            600:{
                items:3
            }
          }
        });
      }
      else {
         $('.team_list').removeClass('owl-carousel');
      }

    });*/

    /// Header_btn /////
    $('.header_nav__btn').on('click', function (event) {
        event.preventDefault();
        var target = $(event.target);
        var menu = $(this).parent();

        $(menu).toggleClass('menu_state_open');
        $(this).toggleClass('nav_btn__active');
        $('body').toggleClass('no_scroll');
    });

    /// End Header_btn /////
    $('.action_text').on('click', function (event) {
        var target = $(event.target);
        $(target).toggleClass('action_text__show');
    });


    $('.action_text').on('click', function (event) {
        var target = $(event.target);
        $(target).toggleClass('action_text__show');
    });

    $('.team_item > a').on('click', function (event) {
        var target = $(event.target);
        var tabsBtn = $('.team_item > a');
        var i;
        for (i = 0; i < tabsBtn.length; i++) {
            var tabsBtnItem = tabsBtn[i];
            $(tabsBtnItem).removeClass('active');
        }
        ;
        $(target).addClass('active');
    });

    function changeClass() {
        if (document.getElementById("promo_2__container").className == "promo_2__container") {
            document.getElementById("promo_2__container").className += " rotated";
        }
        else {
            document.getElementById("promo_2__container").className = "promo_2__container";
        }
    }

    $('.promo_2_link').on('click', function (event) {
        event.preventDefault();
        $('.promo_2_content').toggleClass('d_hidden');
        $('.promo_2_content__mobile').toggleClass('d_hidden');
        $('.promo_2_mobile__info').toggleClass('d_hidden');
        $('.promo_2 > .section_header').toggleClass('d_hidden');
        $('body').toggleClass('no_scroll');
        $('.promo_2__holder').toggleClass('perspective');
        changeClass();
    });


    $('.fn_why__slider').slick({
        dots: false,
        infinite: true,
        //autoplay: true,
        //autoplaySpeed: 3000,
        speed: 3000,
        slidesToShow: 1,
        slidesToScroll: 1,
        dotsClass: 'slick-dots',
    });

    $(".secondary_banner__box").niceScroll({
        cursorcolor: "#45bdb3",
        cursorwidth: "2px",
        cursorborder: "none",
        cursoropacitymin: 1,
    });

    $(document).ready(function () {
        $(".airdrop_mobile__regist_text").on("click", "a", function (event) {
            event.preventDefault();
            var id = $(this).attr('href'),
                top = $(id).offset().top;
            $('body,html').animate({scrollTop: top}, 1500);
        });
    });

    $('#myTab a').on('click', function (e) {
        e.preventDefault()
        $(this).tab('show')
    })


    teamTabs.init();
    teamTabsMobile.init();
    slideModal.init();
    roadmap.init();
    new WOW().init();

});
